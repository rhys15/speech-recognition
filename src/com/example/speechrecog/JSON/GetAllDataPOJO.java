package com.example.speechrecog.JSON;

import java.util.List;

import com.example.speechrecog.database.ActivityHandler;
import com.example.speechrecog.database.VideoHandler;

public class GetAllDataPOJO {
	
	int activity_count;
	List<ActivityHandler> activity;
	List<VideoHandler> lesson;
	
	
	
	public GetAllDataPOJO() {
		// TODO Auto-generated constructor stub
	}
	
	public GetAllDataPOJO(int activity_count, List<ActivityHandler> activity, List<VideoHandler> lesson) {
		this.activity_count = activity_count;
		this.activity = activity;
		this.lesson = lesson;
	}
	public int getActivity_count() {
		return activity_count;
	}
	public void setActivity_count(int activity_count) {
		this.activity_count = activity_count;
	}
	public List<ActivityHandler> getActivity() {
		return activity;
	}
	public void setActivity(List<ActivityHandler> activity) {
		this.activity = activity;
	}
	public List<VideoHandler> getLesson() {
		return lesson;
	}
	public void setLesson(List<VideoHandler> lesson) {
		this.lesson = lesson;
	}
	
}
