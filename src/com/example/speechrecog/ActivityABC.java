package com.example.speechrecog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.speechrecog.R;
import com.example.speechrecog.database.Database;
import com.example.speechrecog.database.ActivityHandler;
import com.example.speechrecog.imageLoader.ImageLoader;
//AnimatorListener
import com.example.speechrecog.utils.AlertDialogManager;

public class ActivityABC extends FragmentParent implements RecognitionListener, OnInitListener,  OnUtteranceCompletedListener{
	View view;
	protected static final int RESULT_SPEECH = 1;
	private static final String TAG = "Speech-To-Text";
	private static final int SPEECH_CODE = 3; 
	private SpeechRecognizer mSpeechRecognizer;
	private Intent mSpeechRecognizerIntent; 
	Database db;
	//static variable
	private static final String RECOGNIZE = "RECOGNIZE";
	private static final String IGNORE = "IGNORE";
	//Quiz
	ArrayList<ActivityHandler> allQuiz;
	ActivityHandler currentLesson;
	AlertDialogManager alert;
	private Bundle bundle;
	ImageLoader imageLoader;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.alphabet_lesson, container, false);
		db = new Database(getActivity());
		alert = new AlertDialogManager();
		imageLoader = new ImageLoader(getActivity());
		//get bundle from another activity
		bundle = this.getArguments();
		//set Widgets
		setWidget();
		return view;
	}
	//on application start
	@Override
	public void onStart() {
		super.onStart();
	}

	private void setWidget() {
		//get all quiz
		allQuiz = db.getAllActivityByType(bundle.getInt("type"));
		//set speech recognizer
		mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
		mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en");

		mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
				getActivity().getPackageName());
		mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
		//		mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
		//		mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		//		mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
		//				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		//		mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
		//				getActivity().getPackageName());
		mSpeechRecognizer.setRecognitionListener(this);
		//set text to speech
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkIntent, SPEECH_CODE);
		//set speak button
		view.findViewById(R.id.speak).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				speak(currentLesson.getInstruction(), true);
			}
		});

		setQuiz();
	}

	ImageView image;
	int counter = 0;
	private void setQuiz() {
		image = (ImageView) view.findViewById(R.id.image);
		if(allQuiz.size() > counter)
			currentLesson = allQuiz.get(counter);
		else
			finalizeQuiz();
//		image.setImageResource(currentLesson.getImage());
		Log.i(TAG, "image to display " + currentLesson.getImage() );
		imageLoader.DisplayImage(currentLesson.getImage(), image, R.drawable.ic_launcher);

	}


	private void finalizeQuiz() {
		alert.showAlertDialog(getActivity(), "Well done", "You've completed the lessn", true);
		switchFrag.goBack(new ActivityABC());
	}

	//text to speech
	private TextToSpeech mTts;
	public void onActivityResult(
			int requestCode, int resultCode, Intent data) {
		if (requestCode == SPEECH_CODE) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				// success, create the TTS instance
				mTts = new TextToSpeech(getActivity(), this);
			} else {
				
				Toast.makeText(getActivity(), "Missing data found! Please install required application to start speech recognition", Toast.LENGTH_LONG).show();;
				// missing data, install it
				Intent installIntent = new Intent();
				installIntent.setAction(
						TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installIntent);
			}
		}
	}

	@Override
	public void onBeginningOfSpeech() {
		Log.i(TAG, "Begin speech ");
//		 Toast.makeText(getActivity(), "Speak now", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEndOfSpeech() {
		Log.i(TAG, "end of speech");
	}

	@Override
	public void onError(int error) {
		Log.i(TAG, "error");
//				 Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPartialResults(Bundle partialResults) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		Toast.makeText(getActivity(), "Voice recording starts", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResults(Bundle results) {
		ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		Toast.makeText(getActivity(), "Result : "+ matches.toString(), Toast.LENGTH_SHORT).show();
		if(!checkIfResultIsCorrect(matches)){
			Animation shake = AnimationUtils.loadAnimation(getActivity(), R.drawable.shake);
			image.startAnimation(shake);
		}
		Log.i(TAG, "Speech Result : " + matches.toString());
		mSpeechRecognizer.stopListening();
	}

	private boolean checkIfResultIsCorrect(ArrayList<String> matches) {
		boolean isCorrect = false;
		for(String match : matches){
			if(currentLesson.getAnswers().contains(match)){
				Log.i(TAG, " Correct " + currentLesson.getAnswers() + " " + match);
				speak("Correct!", false);
				counter++;
				setQuiz();
				isCorrect =true;
				break;
			}
		}
		return isCorrect;
	}
	@Override
	public void onRmsChanged(float rmsdB) {

	}

	@Override
	public void onInit(int status) {
		if(status == TextToSpeech.SUCCESS){
			int result = mTts.setLanguage(Locale.US);
			if(result==TextToSpeech.LANG_MISSING_DATA ||
					result==TextToSpeech.LANG_NOT_SUPPORTED){
				Log.e("error", "This Language is not supported");

			}else{
				mTts.setOnUtteranceCompletedListener(this);
			}
		}
	}

	private void speak(String message, boolean recognize) {
		String tag = IGNORE;
		if(recognize)
			tag = RECOGNIZE;
		HashMap<String, String> myHashAlarm = new HashMap<String, String>();
		myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
		myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, tag);
		mTts.speak(message, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
	}

	@Override
	public void onUtteranceCompleted(final String utteranceId) {
		Log.i(TAG, "callback");
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(RECOGNIZE.equals(utteranceId)){
					mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
				}
			}
		});
	}
}
