package com.example.speechrecog;

import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.speechrecog.database.AboutUsHandler;

public class AboutUsPageFragment extends Fragment{

	private final static String TITLE = "title";
	private final static String ID = "id";
	private final static String CONTENT = "content";
	private final static String IMAGE = "image";
	/* Variables */
	private String title, image, content;
	private int id;
	
	/* Widget */
	private TextView titleLbl, contentLbl;
	
	
	public Fragment newInstance(AboutUsHandler aboutUsHandler) {
		AboutUsPageFragment aboutUsPageFragment = new AboutUsPageFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(ID, aboutUsHandler.getBaseId());
		bundle.putString(TITLE, aboutUsHandler.getTitle());
		bundle.putString(CONTENT, aboutUsHandler.getContent());
		bundle.putString(IMAGE, aboutUsHandler.getDate());
		aboutUsPageFragment.setArguments(bundle);
		return aboutUsPageFragment;
	}
	protected static final String TAG = AboutUs.class.getSimpleName();
	View view;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.about_us_page, container, false);
		Bundle bundle = getArguments();
		title = bundle.getString(TITLE);
		content = bundle.getString(CONTENT);
		id = bundle.getInt(ID);
		
		setWidget();
		setContent();
		setFunction();
		return view;
	}
	private void setWidget() {
		titleLbl = (TextView) view.findViewById(R.id.title);
		contentLbl = (TextView) view.findViewById(R.id.content);
	}
	private void setContent() {
		titleLbl.setText(title);
		contentLbl.setText(content);
	}
	private void setFunction() {
		// TODO Auto-generated method stub
		
	}
	

}
