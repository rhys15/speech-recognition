package com.example.speechrecog;


import com.example.speechrecog.R;
import com.example.speechrecog.database.Database;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;


public class ActivitySelector extends FragmentParent{
	View view;
	Database db;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.activity_selector, container, false);
		db = new Database(getActivity());
		setWidget();
		return view;
	}
	//set widget
	Bundle bundle = new Bundle();
	private void setWidget() {

		view.findViewById(R.id.alphabets).setOnClickListener(new  OnClickListener() {
			@Override
			public void onClick(View v) {
				if(db.checkCategory(ALPHABETS)){
					bundle.putInt("type", ALPHABETS);
					switchFrag.switchFragment(new ActivityABC(), bundle, true);
				}else
					Toast.makeText(getActivity(), "No data found in this Activity", Toast.LENGTH_LONG).show();
			}
		});
		view.findViewById(R.id.numbers).setOnClickListener(new  OnClickListener() {
			@Override
			public void onClick(View v) {
				if(db.checkCategory(NUMBERS)){
					bundle.putInt("type", NUMBERS);
					switchFrag.switchFragment(new ActivityABC(), bundle, true);
				}else
					Toast.makeText(getActivity(), "No data found in this Activity", Toast.LENGTH_LONG).show();
			}
		});
		view.findViewById(R.id.color).setOnClickListener(new  OnClickListener() {
			@Override

			public void onClick(View v) {
				if(db.checkCategory(COLOR)){
					bundle.putInt("type", COLOR);
					switchFrag.switchFragment(new ActivityABC(), bundle, true);
				}else
					Toast.makeText(getActivity(), "No data found in this Activity", Toast.LENGTH_LONG).show();
			}
		});
		view.findViewById(R.id.shapes).setOnClickListener(new  OnClickListener() {
			@Override
			public void onClick(View v) {
				if(db.checkCategory(SHAPES)){
					bundle.putInt("type", SHAPES);
					switchFrag.switchFragment(new ActivityABC(), bundle, true);
				}else
					Toast.makeText(getActivity(), "No data found in this Activity", Toast.LENGTH_LONG).show();
			}
		});
	}
}
