package com.example.speechrecog;

import java.text.SimpleDateFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.example.speechrecog.R;
import com.example.speechrecog.FragmentParent.SwitchFragment;
import com.example.speechrecog.database.Database;
import com.example.speechrecog.database.ActivityHandler;
import com.example.speechrecog.database.VideoHandler;
import com.example.speechrecog.utils.GlobalVariables;

public class MainActivity extends SherlockFragmentActivity implements SwitchFragment, GlobalVariables{

	private static final String TAG = "Main activity";

	Database db = new Database(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//add Alphabets
//		activity_id,  String date_entry, String image, String instruction, String answers, String question, int type
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.a, "What is this letter?", "A, a, hey, hei, ey", "A", ALPHABETS));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.b, "What is this letter?", "b, bee, bi, v, vi", "B", ALPHABETS));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.c, "What is this letter?", "cee, c, see, si", "C", ALPHABETS));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.d, "What is this letter?", "Dee, d, di, tee, tea", "D", ALPHABETS));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.e, "What is this letter?", "e, ee, ei, eh, yee", "E", ALPHABETS));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.f, "What is this letter?", "ef, elf, F, eff", "F",ALPHABETS));
//		//add numbers
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.one, "What is this number?", "one, on, wan", "1", NUMBERS));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.two, "What is this number?", "two, 2, too, tee, tea", "2", NUMBERS));
//		//add color
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.blue, "What is this color?", "blue, blu, flu", "blue", COLOR));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.yellow, "What is this color?", "yellow, yelo, yeelow", "yellow", COLOR));
//		//add shapes
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.square, "What is this shape?", "square, squar, box", "square", SHAPES));
//		db.addActivity(new ActivityHandler(getTimeStomp(), R.drawable.triangle, "What is this shape?", "triangle, angle", "traingle", SHAPES));
		//add video
//		db.addVideo(new VideoHandler(1, getTimeStomp(), "http://www.youtube.com/watch?v=B_OfetgHgBQ/", "90s and 123"));
//		db.addVideo(new VideoHandler(1, getTimeStomp(), "http://www.youtube.com/watch?v=B_OfetgHgBQ", "original"));
		// add quiz to database
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, new Splash()).addToBackStack(null);
		//confirm changes
		transaction.commit();
	}

	@Override
	public void switchFragment(Fragment fragment, Bundle bundle,
			boolean isOnBackStack) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		//set arguemtns to fragment
		fragment.setArguments(bundle);
		//begin transaction
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		if(isOnBackStack)
			transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commitAllowingStateLoss();
	}
	// on back press
	@Override
	public void onBackPressed() {
		FragmentManager fm = getSupportFragmentManager();
		Log.i(TAG, "count " + fm.getBackStackEntryCount());
		if(fm.getBackStackEntryCount() > 2)
			super.onBackPressed();
		else
			return;
	}
	//back listener
	@Override
	public void goBack(Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		Log.i(TAG, "name : " + backStateName);
		getSupportFragmentManager().popBackStackImmediate(backStateName, FragmentManager.POP_BACK_STACK_INCLUSIVE);

	}

	public String getTimeStomp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new java.util.Date());
	}
}
