package com.example.speechrecog.database;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VideoHandler{
	private final static String TAG = "Video";
	int id; 
	@JsonProperty("lesson_id")
	int base_id; 
	@JsonProperty("date_modified")
	String date_entry;
	String link; 
	@JsonProperty("lesson_name")
	String name;

	/**======================= Default Constructor ========================**/

	public VideoHandler(){

	}

	public VideoHandler(int id, int base_id, String date_entry, String link, String name){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.link = link;
		this.name = name;
	}

	public VideoHandler( int base_id, String date_entry, String link, String name){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.link = link;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getLink(){
		return link;
	}
	public void setLink(String link){
		this.link = link;
	}
}