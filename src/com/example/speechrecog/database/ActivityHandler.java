package com.example.speechrecog.database;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActivityHandler{
	private final static String TAG = "Lesson";
	int id; 
	
	int activity_id;
	
	@JsonProperty("date_modified")
	String date_entry; 
	
	
	String image; 
	
	String instruction;
	
	@JsonProperty("combination")
	String answers; 
	
	@JsonProperty("answer")
	String question;
	
	@JsonProperty("activity_category")
	int type;

	/**======================= Default Constructor ========================**/

	public ActivityHandler(){

	}

	public ActivityHandler(int id, int activity_id, String date_entry, String image, String instruction, String answers, String question, int type){
		this.id = id;
		this.activity_id = activity_id;
		this.date_entry = date_entry;
		this.image = image;
		this.instruction = instruction;
		this.answers = answers;
		this.question = question;
		this.type = type;
	}

	public ActivityHandler(int activity_id,  String date_entry, String image, String instruction, String answers, String question, int type){
		this.activity_id = activity_id;
		this.date_entry = date_entry;
		this.image = image;
		this.instruction = instruction;
		this.answers = answers;
		this.question = question;
		this.type = type;
	}

	public int getActivity_id() {
		return activity_id;
	}

	public void setActivity_id(int activity_id) {
		this.activity_id = activity_id;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getImage(){
		return image;
	}
	public void setImage(String image){
		this.image = image;
	}

	public String getInstruction(){
		return instruction;
	}
	public void setInstruction(String instruction){
		this.instruction = instruction;
	}

	public String getAnswers(){
		return answers;
	}
	public void setAnswers(String answers){
		this.answers = answers;
	}

	public String getQuestion(){
		return question;
	}
	public void setQuestion(String question){
		this.question = question;
	}
}