package com.example.speechrecog.database;

public class AboutUsHandler {
	int id, baseId;
	String date, title, content;
	
	public AboutUsHandler() {
		// TODO Auto-generated constructor stub
	}
	public AboutUsHandler(int id, int baseId, String date, String title,
			String content) {
		this.id = id;
		this.baseId = baseId;
		this.date = date;
		this.title = title;
		this.content = content;
	}
	
	public AboutUsHandler(int baseId, String date, String title,
			String content) {
		this.baseId = baseId;
		this.date = date;
		this.title = title;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBaseId() {
		return baseId;
	}

	public void setBaseId(int baseId) {
		this.baseId = baseId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
