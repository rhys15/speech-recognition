package com.example.speechrecog.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper { 
	private static final String database_VoiceTutor = "VoiceTutor"; 
	private static final String table_activity = "lesson"; 

	private static final String key_id = "id";
	private static final String key_date_entry = "date_entry";
	private static final String key_image = "image";
	private static final String key_instruction = "instruction";
	private static final String key_answers = "answers";
	private static final String key_question = "question";
	private static final String key_type = "type";

	String CREATE_ACVIITY_TABLE = "CREATE TABLE " + table_activity+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + "  INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_image + " TEXT, " + 
			key_instruction + " TEXT, " + 
			key_answers + " TEXT, " + 
			key_question + " TEXT , " + 
			key_type + " INTEGER, " + 
			"UNIQUE ( " + key_base_id + ") ON CONFLICT REPLACE);" ;
	public Database(Context context) { 
		super( context, database_VoiceTutor, null, 1); 
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_ACVIITY_TABLE);
		db.execSQL(CREATE_video_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + table_activity);
		db.execSQL("DROP TABLE IF EXISTS " + table_video);
		onCreate(db);
	}
	/** ===================== CRUD ================ **/
	public void addActivity(ActivityHandler lessonHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, lessonHandlerArray.activity_id);
		values.put ( key_date_entry, lessonHandlerArray.date_entry);
		values.put ( key_image, lessonHandlerArray.image);
		values.put ( key_instruction, lessonHandlerArray.instruction);
		values.put ( key_answers, lessonHandlerArray.answers);
		values.put ( key_question, lessonHandlerArray.question);
		values.put ( key_type, lessonHandlerArray.type);
		db.insert( table_activity, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public ActivityHandler getActivity (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_activity, new String[] { 
				key_id, key_base_id,  key_date_entry, key_image, key_instruction, key_answers, key_question, key_type}, key_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		if(cursor != null)
			cursor.moveToFirst();
		ActivityHandler lessonHandlerArray = new ActivityHandler(
				cursor.getInt(0), 
				cursor.getInt(1),
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4), 
				cursor.getString(5), 
				cursor.getString(6),
				cursor.getInt(7)
				);
		db.close();
		return lessonHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public ActivityHandler getLatestActivity (){
		String selectQuery = "SELECT * FROM " + table_activity + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		ActivityHandler lessonHandlerArray = new ActivityHandler(
				cursor.getInt(0), 
				cursor.getInt(1),
				cursor.getString(2), 
				cursor.getString(3), 
				cursor.getString(4), 
				cursor.getString(5), 
				cursor.getString(6),
				cursor.getInt(7)
				);
		db.close();
		return lessonHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< ActivityHandler >getAllActivity (){
		ArrayList< ActivityHandler> lessonList = new ArrayList< ActivityHandler>();
		String selectQuery = "SELECT * FROM " + table_activity + " ORDER BY " + key_id; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ActivityHandler lessonHandlerArray = new ActivityHandler();
				lessonHandlerArray.setId(cursor.getInt(0)); 
				lessonHandlerArray.setActivity_id(cursor.getInt(1));
				lessonHandlerArray.setDate_entry(cursor.getString(2)); 
				lessonHandlerArray.setImage(cursor.getString(3)); 
				lessonHandlerArray.setInstruction(cursor.getString(4)); 
				lessonHandlerArray.setAnswers(cursor.getString(5)); 
				lessonHandlerArray.setQuestion(cursor.getString(6)); 
				lessonHandlerArray.setType(cursor.getInt(7));
				lessonList.add(lessonHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return lessonList;
	}
	
	/** ===================== CHECK IF THIS CATEGORY HAS DATA ================ **/
	public boolean checkCategory (int category){
		String selectQuery = "SELECT * FROM " + table_activity + " WHERE " + key_type + " =  " + category + " ORDER BY " + key_id; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		boolean isNotEmpty = false;
		if(cursor.getCount() > 0){
			isNotEmpty = true;
		}
		db.close();
		return isNotEmpty;
	}
	/** ===================== GET ALL DATA BY TYPE ================ **/
	public ArrayList< ActivityHandler >getAllActivityByType (int type){
		ArrayList< ActivityHandler> lessonList = new ArrayList< ActivityHandler>();
		String selectQuery = "SELECT * FROM " + table_activity  + " WHERE " + key_type + " = " + type + " ORDER BY " + key_question; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ActivityHandler lessonHandlerArray = new ActivityHandler();
				lessonHandlerArray.setId(cursor.getInt(0)); 
				lessonHandlerArray.setActivity_id(cursor.getInt(1));
				lessonHandlerArray.setDate_entry(cursor.getString(2)); 
				lessonHandlerArray.setImage(cursor.getString(3)); 
				lessonHandlerArray.setInstruction(cursor.getString(4)); 
				lessonHandlerArray.setAnswers(cursor.getString(5)); 
				lessonHandlerArray.setQuestion(cursor.getString(6)); 
				lessonHandlerArray.setType(cursor.getInt(7));
				lessonList.add(lessonHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return lessonList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateActivity(ActivityHandler lessonHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, lessonHandlerArray.id);
		values.put ( key_base_id, lessonHandlerArray.activity_id);
		values.put ( key_date_entry, lessonHandlerArray.date_entry);
		values.put ( key_image, lessonHandlerArray.image);
		values.put ( key_instruction, lessonHandlerArray.instruction);
		values.put ( key_answers, lessonHandlerArray.answers);
		values.put ( key_question, lessonHandlerArray.question);
		values.put ( key_type, lessonHandlerArray.type);
		return db.update( table_activity, values, key_id + " =? " ,
				new String []{ String.valueOf(lessonHandlerArray.getId())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteActivity(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_activity, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getActivityCount(){
		String countQuery = "SELECT * FROM " + table_activity;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		db.close();
		return count;
	}
	/**
	 * Video table
	 * 
	 */
	private static final String table_video = "video"; 

	private static final String key_base_id = "base_id";
	private static final String key_link = "link";
	private static final String key_name = "name";

	String CREATE_video_TABLE = "CREATE TABLE " + table_video+ " ( " + 
			key_id + " INTEGER PRIMARY KEY, " + 
			key_base_id + " INTEGER, " + 
			key_date_entry + " TEXT, " + 
			key_link + " TEXT, " + 
			key_name + " TEXT, " + 
			"UNIQUE ( " + key_base_id + " , " + key_link + ") ON CONFLICT REPLACE);" ;

	/** ===================== CRUD ================ **/
	public void addVideo(VideoHandler videoHandlerArray) { 
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put ( key_base_id, videoHandlerArray.base_id);
		values.put ( key_date_entry, videoHandlerArray.date_entry);
		values.put ( key_link, videoHandlerArray.link);
		values.put ( key_name, videoHandlerArray.name);
		db.insert( table_video, null, values);
		db.close();
	}
	/** ===================== GET SINGLE DATA ================ **/
	public VideoHandler getVideo (long id){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query( table_video, new String[] { 
				key_id, key_base_id, key_date_entry, key_link, key_name}, key_id + "=?", 
				new String[]{ String.valueOf(id) } , null, null, null, null);
		if(cursor != null)
			cursor.moveToFirst();
		VideoHandler videoHandlerArray = new VideoHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3),
				cursor.getString(4)
				);
		db.close();
		return videoHandlerArray; 
	}

	/** ===================== GET LATEST DATA ================ **/
	public VideoHandler getLatestVideo (){
		String selectQuery = "SELECT * FROM " + table_video + " ORDER BY " 
				+ key_date_entry + " DESC " + " LIMIT 1 " ;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.getCount() == 0)
			return null;
		cursor.moveToFirst();
		VideoHandler videoHandlerArray = new VideoHandler(
				cursor.getInt(0), 
				cursor.getInt(1), 
				cursor.getString(2), 
				cursor.getString(3),
				cursor.getString(4)
				);
		db.close();
		return videoHandlerArray;
	}

	/** ===================== GET ALL DATA ================ **/
	public ArrayList< VideoHandler >getAllVideo (){
		ArrayList< VideoHandler> videoList = new ArrayList< VideoHandler>();
		String selectQuery = "SELECT * FROM " + table_video + " ORDER BY " + key_date_entry; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				VideoHandler videoHandlerArray = new VideoHandler();
				videoHandlerArray.setId(cursor.getInt(0)); 
				videoHandlerArray.setBase_id(cursor.getInt(1)); 
				videoHandlerArray.setDate_entry(cursor.getString(2)); 
				videoHandlerArray.setLink(cursor.getString(3)); 
				videoHandlerArray.setName(cursor.getString(4));
				videoList.add(videoHandlerArray);
			} while (cursor.moveToNext());
		}

		db.close();
		return videoList;
	}
	/** ===================== UPDATE DATA ================ **/
	public int updateVideo(VideoHandler videoHandlerArray) {
		SQLiteDatabase db = this.getWritableDatabase(); 
		ContentValues values = new ContentValues(); 
		values.put ( key_id, videoHandlerArray.id);
		values.put ( key_base_id, videoHandlerArray.base_id);
		values.put ( key_date_entry, videoHandlerArray.date_entry);
		values.put ( key_link, videoHandlerArray.link);
		values.put ( key_name, videoHandlerArray.name);
		return db.update( table_video, values, key_base_id + " =? " ,
				new String []{ String.valueOf(videoHandlerArray.getBase_id())});
	}
	/** ===================== DELETE DATA ================ **/
	public void deleteVideo(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(table_video, key_id + " = ? ", new String [] { String.valueOf(id) });
		db.close();
	}
	/** ===================== GET COUNT ================ **/
	public int getVideoCount(){
		String countQuery = "SELECT * FROM " + table_video;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		return cursor.getCount();
	}

}