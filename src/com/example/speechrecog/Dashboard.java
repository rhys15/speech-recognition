package com.example.speechrecog;


import com.example.speechrecog.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;


public class Dashboard extends FragmentParent{
	View view;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.dashboard, container, false);
		setWidget();
		return view;
	}
	//set widget
	private void setWidget() {
		view.findViewById(R.id.activity).setOnClickListener(new  OnClickListener() {

			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new ActivitySelector(), null, true);
			}
		});
		view.findViewById(R.id.lesson).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new VideoLesson(), null, true);
			}
		});
		view.findViewById(R.id.about_us).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new AboutUs(), null, true);
			}
		});
		view.findViewById(R.id.help).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switchFrag.switchFragment(new AboutUs(), null, true);
			}
		});
	}
}
