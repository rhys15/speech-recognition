package com.example.speechrecog.adapter;

import java.util.ArrayList;

import com.example.speechrecog.AboutUsPageFragment;
import com.example.speechrecog.database.AboutUsHandler;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class AboutUsPageAdapter extends FragmentPagerAdapter{
	ArrayList<AboutUsHandler> aboutUsHandlers = new ArrayList<AboutUsHandler>();
	public AboutUsPageAdapter(FragmentManager fm, ArrayList<AboutUsHandler> aboutUsHandlers) {
		super(fm);
		this.aboutUsHandlers  = aboutUsHandlers;
	}

	@Override
	public Fragment getItem(int index) {
		return new AboutUsPageFragment().newInstance(aboutUsHandlers.get(index));
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aboutUsHandlers.size();
	}

}
