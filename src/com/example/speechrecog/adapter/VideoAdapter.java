package com.example.speechrecog.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.provider.MediaStore.Video;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.speechrecog.R;
import com.example.speechrecog.database.Database;
import com.example.speechrecog.database.VideoHandler;


@SuppressWarnings("unused")
public class VideoAdapter extends BaseAdapter{

	private Context activity;
	private ArrayList<VideoHandler> data;
	private static LayoutInflater inflater = null;
	View vi;
	Database db;
	private final static String TAG = "Instructions Adapter";

	public VideoAdapter(Context context, ArrayList<VideoHandler> imageArry) {
		activity = context;
		data = imageArry;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new  Database(context);
	}
	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		vi = convertView;
		final int pos = position;
		vi = inflater.inflate(R.layout.list_item, null);
		//
		TextView title = (TextView)vi.findViewById(R.id.title); // title
		// Setting all values in listview
		VideoHandler video = data.get(pos);
		title.setText(video.getName());
		return vi;
	}


}
