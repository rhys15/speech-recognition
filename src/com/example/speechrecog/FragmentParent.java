package com.example.speechrecog;



import com.actionbarsherlock.app.SherlockFragment;
import com.example.speechrecog.utils.GlobalVariables;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class FragmentParent extends SherlockFragment implements GlobalVariables{
	SwitchFragment switchFrag;
	
	
	public interface SwitchFragment{
		public void switchFragment(Fragment fragment, Bundle bundle, boolean isOnBackStack);
		public void goBack(Fragment fragment);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		switchFrag = (SwitchFragment) activity;
	}
}
