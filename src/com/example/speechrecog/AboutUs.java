package com.example.speechrecog;

import java.util.ArrayList;

import com.example.speechrecog.R;
import com.example.speechrecog.adapter.AboutUsPageAdapter;
import com.example.speechrecog.database.AboutUsHandler;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ViewFlipper;


public class AboutUs extends FragmentParent{
	protected static final String TAG = AboutUs.class.getSimpleName();
	View view;
	/* Variables */
	private AboutUsPageAdapter aboutUsPageAdapter;
	/* Widget */
	private ViewPager viewPager;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.about_us, container, false);
		
		setWidget();
		setContent();
		setFunction();
		return view;
	}
	
	private void setWidget() {
		viewPager = (ViewPager) view.findViewById(R.id.pager);
	}
	private void setContent() {
		ArrayList<AboutUsHandler> aboutUsHandlers = new ArrayList<AboutUsHandler>();
		aboutUsHandlers.add(new AboutUsHandler(1, "date", "title", "content"));		
		aboutUsHandlers.add(new AboutUsHandler(1, "date", "title", "content"));		
		aboutUsHandlers.add(new AboutUsHandler(1, "date", "title", "content"));		
		aboutUsHandlers.add(new AboutUsHandler(1, "date", "title", "content"));		
		aboutUsPageAdapter = new AboutUsPageAdapter(getChildFragmentManager(), aboutUsHandlers);
		viewPager.setAdapter(aboutUsPageAdapter);
	}
	private void setFunction() {
		// TODO Auto-generated method stub
		
	}
}
