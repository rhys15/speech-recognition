package com.example.speechrecog;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.speechrecog.R;
import com.example.speechrecog.adapter.VideoAdapter;
import com.example.speechrecog.database.Database;
import com.example.speechrecog.database.VideoHandler;


public class VideoLesson extends FragmentParent{
	View view;
	Database db;
	ArrayList<VideoHandler> videos;
	ListView list;
	VideoAdapter adapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.video_lesson, container, false);
		db = new Database(getActivity());
		setWidget();
		return view;
	}
	//set widget
	private void setWidget() {
		videos = db.getAllVideo();
		list = (ListView) view.findViewById(R.id.list);
		adapter = new VideoAdapter(getActivity(), videos);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				VideoHandler video = videos.get(position);
				String videoUrl = video.getLink();
//				Intent videoIntent =new Intent(Intent.ACTION_VIEW);
//				videoIntent.setDataAndType(Uri.parse(videoUrl), "video/*");
//				getActivity().startActivity(videoIntent);
				getActivity().startActivity(getYouTubeIntent(getActivity(), videoUrl));
			}
			
		});
	}
	
	
	public static Intent getYouTubeIntent(Context context, String url) {
		  Intent videoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		  final PackageManager pm = context.getPackageManager();
		  List<ResolveInfo> activityList = pm.queryIntentActivities(videoIntent, 0);
		  for (int i = 0; i < activityList.size(); i++) {
		    ResolveInfo app = activityList.get(i);
		    if (app.activityInfo.name.contains("youtube")) {
		      videoIntent.setClassName(app.activityInfo.packageName, app.activityInfo.name);
		      return videoIntent;
		    }
		  }
		  return videoIntent;
		}
}
