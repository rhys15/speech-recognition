package com.example.speechrecog.utils;

public interface GlobalVariables {
	public final static int ALPHABETS = 1;
	public final static int NUMBERS = 2;
	public final static int COLOR = 3;
	public final static int SHAPES = 4;
	public final static int WORDS = 5;
	public final static String GET_ALL_URL ="http://krolem.worklude.com//toddler/api/fetchData/";
}
