package com.example.speechrecog;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.speechrecog.R;
import com.example.speechrecog.JSON.GetAllDataPOJO;
import com.example.speechrecog.JSON.JSONParser;
import com.example.speechrecog.database.ActivityHandler;
import com.example.speechrecog.database.Database;
import com.example.speechrecog.database.VideoHandler;
import com.example.speechrecog.utils.GlobalVariables;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;


public class Splash extends FragmentParent implements GlobalVariables{
	public static final String TAG = "Splash";
	View view;
	Database db;
	JSONParser jsonParser = new JSONParser();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//set layout 
		view = inflater.inflate(R.layout.splash, container, false);
		db = new Database(getActivity());
		new LoginOnline().execute();
		return view;
	}

	/** ===================================================================
	 * 
	 * LOGIN ONLINE
	 * @author user
	 *
	 ======================================================================*/
	class LoginOnline extends AsyncTask<String, Integer, String> {
		boolean isLogin;
		int employeeID;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.i(TAG, "Execute Asynctask");
		}

		@Override
		protected String doInBackground(String... account) {

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			String activityLatestDate = "0000-00-00 00:00:00";
			String lessonLatestDate = "0000-00-00 00:00:00";

			ActivityHandler activity = db.getLatestActivity();
			if(activity!=null)
				activityLatestDate = activity.getDate_entry();
			VideoHandler videos = db.getLatestVideo();
			if(videos != null)
				activityLatestDate = videos.getDate_entry();
			params.add(new BasicNameValuePair("date", activityLatestDate+"|||"+lessonLatestDate));
		
			String jsonData = jsonParser.makeHttpRequest(GET_ALL_URL, "POST", params);
			Log.i(TAG, "json data " + jsonData);
			Log.i(TAG, "parameter data " + params.toString() + " url " + GET_ALL_URL);
			parseJacksonPOJO(jsonData);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			switchFrag.switchFragment(new Dashboard(), null, true);
		}
	}

	@SuppressWarnings("deprecation")
	private  void parseJacksonPOJO(String jsonString) {	
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory f = new JsonFactory();
			JsonParser jp = f.createJsonParser(jsonString);
			GetAllDataPOJO data = mapper.readValue(jsonString, GetAllDataPOJO.class);
			for(ActivityHandler item : data.getActivity()){
				db.addActivity(item);
				Log.i(TAG, "answer " + item.getAnswers());
			}
			for(VideoHandler item : data.getLesson()){
				db.addVideo(item);
				Log.i(TAG, "link : " + item.getLink());
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
